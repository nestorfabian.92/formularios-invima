<?php

/********************************************************************************
 AppForm invima

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

Dibuja el pdf de alertas y informes

 ********************************************************************************/


require 'includes/init.php';
require 'config.php';
require 'includes/db-core.php';
require 'includes/helper-functions.php';
require 'includes/check-session.php';

require 'includes/language.php';
require 'includes/entry-functions.php';
require 'includes/post-functions.php';
require 'includes/users-functions.php';
require 'lib/dompdf/dompdf_config.inc.php';

$form_id  = (int) trim($_GET['form_id']);
$entry_id = (int) trim($_GET['entry_id']);

if (empty($form_id) || empty($entry_id)) {
	die("Invalid Request");
}

$dbh         = mf_connect_db();
$mf_settings = mf_get_settings($dbh);

//check permission, is the user allowed to access this page?
if (empty($_SESSION['mf_user_privileges']['priv_administer'])) {
	$user_perms = mf_get_user_permissions($dbh, $form_id, $_SESSION['mf_user_id']);

	//this page need edit_entries or view_entries permission
	if (empty($user_perms['edit_entries']) && empty($user_perms['view_entries'])) {
		$_SESSION['MF_DENIED'] = "You don't have permission to access this page.";

		$ssl_suffix = mf_get_ssl_suffix();
		header("Location: http{$ssl_suffix}://" .$_SERVER['HTTP_HOST'].mf_get_dirname($_SERVER['PHP_SELF'])."/restricted.php");
		exit;
	}
}

$template_data_options = array();

$template_data_options['as_plain_text']      = false;
$template_data_options['target_is_admin']    = true;
$template_data_options['machform_path']      = $mf_settings['base_url'];
$template_data_options['show_image_preview'] = true;
$template_data_options['use_list_layout']    = true;

$template_data = mf_get_template_variables($dbh, $form_id, $entry_id, $template_data_options);

$template_variables = $template_data['variables'];
$template_values    = $template_data['values'];

//print_r($template_variables);
//var_dump($template_values[33]);
//$porciones = explode(" ", $template_values[29]);

// echo $porciones[0]; // porción1
// echo $porciones[1]; // porción2

$img_alerta = $template_values[32];

$link_archivo = (string) $template_values[33];

$imagen_alerta = str_replace("https://app.invima.gov.co", "http://172.16.10.219", $img_alerta);

$link_archivos = str_replace("http://172.16.10.219", "https://app.invima.gov.co", $link_archivo);

$link_archi = (string) $template_values[20];

$link_archi = str_replace("http://172.16.10.219", "https://app.invima.gov.co", $link_archi);

/*print_r($template_values);

 */

/*
var_dump($template_values);
exit();
*/


if ($template_values[45] == "Informes_seguridad" or $template_values[47] == "Informes_seguridad") {

	/* inicio de variable de Informes de seguridad */

	$pdf_content = '<html>

								<head>
										<style type="text/css">
											.alerta_img img{
												width: 260px;
												height: auto;
												position:relative;

											}
											.alerta_img a{
												display:none;
											}

											.alerta_img_cg img{
												width: 560px;
											}

											.alerta_img_cg a{
												display:none;
											}

											.img-encabezado{
												position:absolute;
												left:-46px;
												right:0;
												top:-46px;
												width:796px;
											}

											h3{
												font-weight:none;
											}
                                            div{
                                                position:relative;
                                            }

                                            hr{
                                                color:#f7f9f9;
                                                width:100%;
                                            }

                                            a{
                                            	color:#11576C !important;
                                            }

										</style>
								</head>
								<body>

											<div>';

	$pdf_content .= '
											<img  align="left" width="550px"  src="./images/alertas_img/img_encabezado.png">

													<p> &nbsp; </p>
													<p> &nbsp; </p>
													<p> &nbsp; </p>

													<h1 align="center"><font color="004a84"><u>Informe de seguridad</u></font></h1>
													<h2 align="center"><font color="004a84">'.$template_values[0].'</font></h1>
													<!-- <font color="21babc"><hr size="5" noshade="noshade"></font> -->

											';

	$pdf_content .= '
													<!-- <p align="right" style="bottom:-20px;">No. Identificación interno. '.$template_values[1].'</p> -->


													<p align="right" style="bottom:-20px;">Informe de Seguridad No. '.$template_values[7].'</p>
													<p align="right" style="position:relative;top:-16px;">Bogotá, '.$template_values[9].'</p>
													';

	if (empty($template_values[11])) {

		$pdf_content .= '
																	<h3 align="left">
																	'.$template_values[11].'
																		';

		$pdf_content .= '
																	</h3>
																		<p>
																		';

	} else {
		$pdf_content .= '
															<h3 align="left">
															El Instituto Nacional de Vigilancia de Medicamentos y Alimentos (Invima) comunica la siguiente información de seguridad:
															';

		$pdf_content .= '
																</h3>


															<p>

															<p>
															';
	}
	$pdf_content .= '
                                                        <hr>
                                            <p>
													'
	;
	if (empty($template_values[6])) {
		# code...
	} else {
		# code...
		$pdf_content .= '

														<div >
															<div><strong>Asunto:</strong> '.$template_values[6].'</div>
                                                        </div>
                                                        <p></p>
															';
	}
	if (empty($template_values[13])) {
		# code...
	} else {
		# code...
		$pdf_content .= '
                                                    <div>
                                                        <div><strong>Principio Activo:</strong>'.$template_values[13].'</div>
                                                    </div>
                                                    <p></p>
															';
	}

	if (empty($template_values[7])) {
		# code...
	} else {
		# code...
		$pdf_content .= '
                                                    <div>
														<div><strong>No. identificación interna del Informe de Seguridad:</strong> '.$template_values[8].'</div>
                                                    </div>
                                                    <p></p>
															';
	}

	if (empty($template_values[14])) {
		# code...
	} else {
		# code...
		$pdf_content .= '
                                                    <div>

																<div><strong>Registro Sanitario:</strong> '.$template_values[14].'</div>
                                                    </div>
                                                    <p></p>
															';
	}

	if (empty($template_values[15])) {
		# code...
	} else {
		# code...
		$pdf_content .= '
                                                    <div>

																<div><strong>Presentación Comercial:</strong> '.$template_values[15].'</div>
                                                            </div>
                                                    <p></p>
															';
	}

	if (empty($template_values[16])) {
		# code...
	} else {
		# code...
		$pdf_content .= '

                                                        <div>

																<div><strong>Fabricante / importador</strong> '.$template_values[16].'   </div>
                                                        </div>
                                                    <p></p>
															';
	}

	if (empty($template_values[17])) {
		# code...
	} else {
		# code...
		$pdf_content .= '

																<div>
																    <div><strong>Lote / Serial</strong> '.$template_values[17].'</div>
                                                                </div>
                                                                <p></p>
															';
	}

	if (empty($template_values[18])) {
		# code...
	} else {
		# code...
		$pdf_content .= '
                                                    <div>
																<div><strong>Referencia</strong> '.$template_values[18].'</div>
                                                    </div>
                                                    <p></p>

															';
	}

	if (empty($template_values[19])) {
		# code...
	} else {
		# code...
		$pdf_content .= '

													<div>
																<div><strong>Mecanismo de acción</strong> '.$template_values[19].'</div>
                                                    </div>
                                                    <p></p>
															';
	}

	if (empty($template_values[20])) {
		# code...
	} else {
		# code...
		$pdf_content .= '

													<div>
																<div><strong>Enlace Relacionado</strong> '.$link_archi.'</div>
                                                    </div>
                                                    <br>
                                                    <hr>
                                                    <p></p>
															';
	}

	$pdf_content .= '
                                       <br>
                                       <br>

												</p>


														';

	if (isset($template_values[21])) {
		# code...
	} else {
		# code...
		// imagen
		$pdf_content .= '

																	<center>
																	<div class="alerta_img" style="list-style-type: none;margin: 0;padding: 0;width: 100%;font-family: dejavu sans,Lucida Grande,Tahoma,Arial,Verdana,sans-serif;font-size:9.2pt;position:relative;">'.$template_values[21].'
																	</div>
																	</center>
																	<p>
													';
	}

	if (empty($template_values[22])) {
		# code...
	} else {
		# code...

		//li 1 gris
		$pdf_content .= '

																	<p><strong>Descripción del caso</strong></p>
																	<p>
																	<div align="Justify" style="width: 100%;padding:5px 0px">
																	'.$template_values[22].'
																	</div>

											';
	}

	/********************  DM-PF1  ********************/

	if (empty($template_values[23])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																		<p><strong>Antecedentes</strong></p>
																		<p>
																		<div align="Justify" style="width: 100%;padding:5px 0px">
																		'.$template_values[23].'
																		</div>
												';

	}

	if (empty($template_values[24])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																		<p><strong>Análisis y conclusiones</strong></p>
																		<p>
																		<div align="Justify" style="width: 100%;padding:5px 0px">
																		'.$template_values[24].'
																		</div>
												';

	}

	if (isset($template_values[25])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																		<p><strong>Producto importado a colombia</strong></p>
																		<p>
																		<div align="Justify" style="width: 100%;padding:5px 0px">
																		'.$template_values[25].'
																		</div>
												';

	}

	if (empty($template_values[26])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																		<p><strong>Acciones tomadas por el Invima</strong></p>
																		<p>
																		<div align="Justify" style="width: 100%;padding:5px 0px">
																		'.$template_values[26].'
																		</div>
												';

	}

	if (empty($template_values[27])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																		<p><strong>Información para profesionales de la salud</strong></p>
																		<p>
																		<div align="Justify" style="width: 100%;padding:5px 0px">
																		'.$template_values[27].'
																		</div>
												';

	}

	if (empty($template_values[28])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																		<p><strong>Información para pacientes y cuidadores</strong></p>
																		<p>
																		<div align="Justify" style="width: 100%;padding:5px 0px">
																		'.$template_values[28].'
																		</div>
												';

	}

	if (empty($template_values[29])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																		<p><strong>Información para IPSs y EAPBs</strong></p>
																		<p>
																		<div align="Justify" style="width: 100%;padding:5px 0px">
																		'.$template_values[29].'
																		</div>
												';

	}

	if (empty($template_values[30])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																		<p><strong>Información para establecimientos</strong></p>
																		<p>
																		<div align="Justify" style="width: 100%;padding:5px 0px">
																		'.$template_values[30].'
																		</div>
												';

	}

	if (empty($template_values[31])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																		<p><strong>Información para Entidades Territoriales de Salud</strong></p>
																		<p>
																		<div align="Justify" style="width: 100%;padding:5px 0px">
																		'.$template_values[31].'
																		</div>
												';

	}

	if (empty($template_values[32])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																		<p><strong>Información para la Red Nacional de Farmacovigilancia</strong></p>
																		<p>
																		<div align="Justify" style="width: 100%;padding:5px 0px">
																		'.$imagen_alerta.'
																		</div>
												';

	}

	if (empty($template_values[33])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																		<p><strong>Información para la Red Nacional de Tecnovigilancia</strong></p>
																		<p>
																		<div align="Justify" style="width: 100%;padding:5px 0px">
																		'.$link_archivos.'
																		</div>
												';

	}

	if (empty($template_values[34])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																		<p><strong>Información para la Red Nacional de Reactivovigilancia</strong></p>
																		<p>
																		<div align="Justify" style="width: 100%;padding:5px 0px">
																		'.$template_values[34].'
																		</div>
												';

	}

	if (empty($template_values[35])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																		<p><strong>Fuentes de información</strong></p>
																		<p>
																		<div align="Justify" style="width: 100%;padding:5px 0px">
																		'.$template_values[35].'
																		</div>
												';

	}

	if (empty($template_values[36])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																		<p><strong>Referencias Bibliográficas</strong></p>
																		<p>
																		<div align="Justify" style="width: 100%;padding:5px 0px">
																		'.$template_values[36].'
																		</div>
												';

	}

	if (empty($template_values[37])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																		<p><strong>Si desea obtener mayor información comuníquese con el Invima a los siguientes correos electrónicos.</strong></p>
																		<p>
																		<div align="Justify" style="width: 100%;padding:5px 0px">
																		'.$template_values[37].'
																		</div>
												';

	}

	if (empty($template_values[38])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '


																		<p>
																		<div align="Justify" style="width: 100%;padding:5px 0px">
																		'.$template_values[38].'
																		</div>
												';

	}

	if (empty($template_values[39])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																		<p><strong>Realizar peticiones, quejas, reclamos, denuncias o sugerencias: </strong></p>
																		<p>
																		<div align="Justify" style="width: 100%;padding:5px 0px">
																		'.$template_values[39].'
																		</div>
												';

	}

	if (empty($template_values[40])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																		<p><strong>Consultar registros sanitarios: </strong></p>
																		<p>
																		<div align="Justify" style="width: 100%;padding:5px 0px">
																		'.$template_values[40].'
																		</div>
												';

	}

	if (empty($template_values[41])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																		<p><strong>Reporte eventos adversos: </strong></p>
																		<p>
																		<div align="Justify" style="width: 100%;padding:5px 0px">
																		'.$template_values[41].'
																		</div>
												';

	}

	if (empty($template_values[42])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																		<p><strong>Farmacovigilancia: </strong></p>
																		<p>
																		<div align="Justify" style="width: 100%;padding:5px 0px">
																		'.$template_values[42].'
																		</div>
												';

	}

	if (empty($template_values[43])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																		<p><strong>Tecnovigilancia: </strong></p>
																		<p>
																		<div align="Justify" style="width: 100%;padding:5px 0px">
																		'.$template_values[43].'
																		</div>
												';

	}

	if (empty($template_values[44])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																		<p><strong>Reactivovigilancia: </strong></p>
																		<p>
																		<div align="Justify" style="width: 100%;padding:5px 0px">
																		'.$template_values[44].'
																		</div>
												';

	}

	$pdf_content .= '

														</ul>
														<br>
														<br>
														<br>
														<br>
														<br>
													    <br>
													    <br>
														<br>


														';

	$pdf_content .= '
									</body>
								</html>
										';
	// Variable para imprimir {entry_data}

	$pdf_content_footer = '

												<img style="position: absolute; bottom:50; width:99%;" align="center" width="100%"  src="./images/alertas_img/img_pie.png">

									';

	/* fin de variable de Informes de seguridad */

	//parse pdf template
	$pdf_content = str_replace($template_variables, $template_values, $pdf_content);

	//print_r($pdf_content);

	//exit();
	//generate PDF file
	$dompdf = new DOMPDF();

	//paper size: letter, legal, ledger, tabloid, executive, folio, a0, a1, a2, a3, a4,a5, a6, etc
	//orientation: portrait, landscape
	$dompdf->set_paper('A4', 'portrait');

	$html_pdf = $pdf_content;

	// print_r($pdf_content);
	// exit();

	$dompdf->load_html($html_pdf);

	$dompdf->render();

	$canvas = $dompdf->get_canvas();

	//For header
	$header = $canvas->open_object();
	$font   = Font_Metrics::get_font("Arial", "bold");
	$date   = date("Y-m-d H:i:s");
	//$canvas->page_text(35, 25, "Invima", $font, 8, array(0, 0, 0));
	//$canvas->page_text(470, 25, "Alerta Invima No. $template_values[1]", $font, 8, array(0, 0, 0));
	$canvas->close_object();
	$canvas->add_object($header, "all");

	//For Footer
	$footer = $canvas->open_object();
	$font   = Font_Metrics::get_font("Arial", "bold");
	$date   = date("Y-m-d H:i:s");
	$image  = "./images/alertas_img/img_pie.png";
	$canvas->image($image, 0, 720, 600, 80);
	$canvas->page_text(250, 800, "Página: {PAGE_NUM} de {PAGE_COUNT}", $font, 8, array(0, 0, 0));
	$canvas->page_text(240, 820, "Informe de seguridad No. $template_values[7]", $font, 8, array(0, 0, 0));
	//$canvas->page_text(500, 800, $date, $font, 8, array(0, 0, 0));
	$canvas->close_object();
	$canvas->add_object($footer, "all");

	//$dompdf->stream("Alerta Sanitaria Numero #{$entry_id} - Form #{$form_id}.pdf");
	$dompdf->stream("Informe de seguridad No. #$template_values[7] .pdf");

}

///////////////// ALertas Sanitarias Formato ///////////////////////////////////////////////////////

if ($template_values[139] == "Alertas_sanitarias") {

	/* inicio de variable de alerta sanitaria */

	$productos = $template_values[18];

	$producto = explode(";", $productos);

	$pdf_content = '<html>

								<head>
										<style type="text/css">
											.alerta_img img{
												width: 260px;
												height: auto;
												position:relative;

											}
											.alerta_img a{
												display:none;
											}

											.alerta_img_cg img{
												width: 560px;
											}

											.alerta_img_cg a{
												display:none;
											}

											.img-encabezado{
												position:absolute;
												left:-46px;
												right:0;
												top:-46px;
												width:796px;
											}

											h3{
												font-weight:none;
											}

											a{
                                            	color:#11576C !important;
                                            }

										</style>
								</head>
								<body>

											<div>

													<img  align="left" width="550px"  src="./images/alertas_img/img_encabezado.png">

													<p> &nbsp; </p>
													<p> &nbsp; </p>
													<p> &nbsp; </p>

													<h1 align="center"><font color="004a84"><strong>ALERTA SANITARIA</strong></font></h1>
													<h2 align="center"><font color="004a84">'.$template_values[0].'</font></h2>
													<!-- <font color="004a84"><hr size="5" noshade="noshade"></font> -->

													<!-- <p align="right" style="bottom:-20px;">No. Identificación interno. '.$template_values[1].'</p> -->


													<p align="right" style="bottom:-20px;">Alerta No. '.$template_values[1].'</p>
													<p align="right" style="position:relative;top:-16px;">Bogotá, '.$template_values[3].'</p>
													';

	if (empty($template_values[17])) {

		$pdf_content .= '
																	<h3 align="center">
																	<b>Invima alerta
																';

		$pdf_content .= '</b>
																		</h3>


																		<p>
																		';

	} else {

		$pdf_content .= '
															<p></p>
															<p align="center" style="font-size:14pt"><strong><center>
															'.$template_values[17].'
															</center></strong>
															</p>
															<p>
															<p>
						 					                ';
	}

	if (empty($template_values[15])) {
		# code...
		$pdf_content .= '
														<p align="Justify"> '.$template_values[16].'</p>
														<p></p>
														';

	} else {

		$pdf_content .= '
														<p align="Justify"> '.$template_values[15].'</p>
														<p></p>
														';

	}

	$pdf_content .= '<hr><p>';

	if (empty($template_values[17])) {
		# code...
	} else {

		$pdf_content .= '

															<div>
																<div><strong>Nombre del producto:</strong>  '.$template_values[17].'</div>
	                                          </div>
	                                          <p></p>


								    ';
	}

	if (empty($template_values[18])) {
		# code...
	} else {

		$pdf_content .= '


									<div style="position:relative; margin-bottom:5px; margin-top:16px;" >

														<ul style="list-style-type: none;margin: 0;padding: 0;width: 100%;font-family: dejavu sans,Lucida Grande,Tahoma,Arial,Verdana,sans-serif;font-size:9.2pt;position:relative; font-weight:600;">

															<li style="background-color:#fff;overflow: auto; border:1px solid #DEDEDE;clear:both">
																<div style="padding:3px;">
																	'.$producto[0].'
																</div>
															</li>
															<li style="background-color:#fff;overflow: auto; border:1px solid #DEDEDE;clear:both">
																<div style="padding:3px;">
																	'.$producto[1].'
																</div>
															</li>
															<li style="background-color:#fff;overflow: auto; border:1px solid #DEDEDE;clear:both">
																<div style="padding:3px;">
																	'.$producto[2].'
																</div>
															</li>
															<li style="background-color:#fff;overflow: auto; border:1px solid #DEDEDE;clear:both">
																<div style="padding:3px;">
																	'.$producto[3].'
																</div>
															</li>
															<li style="background-color:#fff;overflow: auto; border:1px solid #DEDEDE;clear:both">
																<div style="padding:3px;">
																	'.$producto[4].'
																</div>
															</li>
															<li style="background-color:#fff;overflow: auto; border:1px solid #DEDEDE;clear:both">
																<div style="padding:3px;">
																	'.$producto[5].'
																</div>
															</li>
															<li style="background-color:#fff;overflow: auto; border:1px solid #DEDEDE;clear:both">
																<div style="padding:3px;">
																	'.$producto[6].'
																</div>
															</li>
															<li style="background-color:#fff;overflow: auto; border:1px solid #DEDEDE;clear:both">
																<div style="padding:3px;">
																	'.$producto[7].'
																</div>
															</li>
															<li style="background-color:#fff;overflow: auto; border:1px solid #DEDEDE;clear:both">
																<div style="padding:3px;">
																	'.$producto[8].'
																</div>
															</li>
															<li style="background-color:#fff;overflow: auto; border:1px solid #DEDEDE;clear:both">
																<div style="padding:3px;">
																	'.$producto[9].'
																</div>
															</li>
															<li style="background-color:#fff;overflow: auto; border:1px solid #DEDEDE;clear:both">
																<div style="padding:3px;">
																	'.$producto[10].'
																</div>
															</li>
															<li style="background-color:#fff;overflow: auto; border:1px solid #DEDEDE;clear:both">
																<div style="padding:3px;">
																	'.$producto[11].'
																</div>
															</li>
															<li style="background-color:#fff;overflow: auto; border:1px solid #DEDEDE;clear:both">
																<div style="padding:3px;">
																	'.$producto[12].'
																</div>
															</li>
															<li style="background-color:#fff;overflow: auto; border:1px solid #DEDEDE;clear:both">
																<div style="padding:3px;">
																	'.$producto[13].'
																</div>
															</li>
															<li style="background-color:#fff;overflow: auto; border:1px solid #DEDEDE;clear:both">
																<div style="padding:3px;">
																	'.$producto[14].'
																</div>
															</li>
															<li style="background-color:#fff;overflow: auto; border:1px solid #DEDEDE;clear:both">
																<div style="padding:3px;">
																	'.$producto[15].'
																</div>
															</li>

														<ul>
													</div>
									';

	}

	if (empty($template_values[20])) {
		# code...
	} else {
		# code...
		$pdf_content .= '
															<div>
																<div><strong>Registro sanitario:</strong>  '.$template_values[20].'</div>
	                                          </div>
	                                          <p></p>

															';

	}

	if (empty($template_values[21])) {
		# code...
	} else {
		# code...
		$pdf_content .= '

															<div>
																<div><strong>Principio Activo:</strong>  '.$template_values[21].'</div>
	                                          </div>
	                                          <p></p>

															';
	}

	if (empty($template_values[22])) {
		# code...
	} else {
		# code...
		$pdf_content .= '

															<div>
																<div><strong>Presentación comercial:</strong>  '.$template_values[22].'</div>
	                                          </div>
	                                          <p></p>

															';
	}

	if (empty($template_values[23])) {
		# code...
	} else {

		$pdf_content .= '

															<div>
																<div><strong>Titular del registro:</strong>  '.$template_values[23].'</div>
	                                          </div>
	                                          <p></p>

															';
	}

	if (empty($template_values[24])) {
		# code...
	} else {

		$pdf_content .= '

															<div>
																<div><strong>Fabricante(s) / Importador(es):</strong>  '.$template_values[24].'</div>
	                                          </div>
	                                          <p></p>
									';
	}

	if (empty($template_values[25])) {
		# code...
	} else {

		$pdf_content .= '

															<div>
																<div><strong>Referencia(s) / Código(s):</strong>  '.$template_values[25].'</div>
	                                          </div>
	                                          <p></p>
									';
	}

	if (empty($template_values[26])) {
		# code...
	} else {

		$pdf_content .= '
															<div>
																<div><strong>Lote(s) / Serial(es):</strong>  '.$template_values[26].'</div>
	                                          </div>
	                                          <p></p>
														';
	}

	if (empty($template_values[27])) {
		# code...
	} else {

		$pdf_content .= '

															<div>
																<div><strong>Fecha de producción:</strong>  '.$template_values[27].'</div>
	                                          </div>
	                                          <p></p>
														';

	}

	if (empty($template_values[28])) {
		# code...
	} else {

		$pdf_content .= '
													<!--

															<div>
																<div><strong>FFecha de vencimiento:</strong>  '.$template_values[28].'</div>
	                                          </div>
	                                          <p></p>

													-->
														';

	}

	if (empty($template_values[29])) {
		# code...
	} else {

		$pdf_content .= '
															<div>
																<div><strong>Concentración total de mercurio:</strong>  '.$template_values[29].'</div>
	                                          </div>
	                                          <p></p>
														';

	}

	if (empty($template_values[30])) {
		# code...
	} else {

		$pdf_content .= '

															<div>
																<div><strong>Fuente de la alerta:</strong>  '.$template_values[30].'</div>
	                                          </div>
	                                          <p></p>
														';

	}

	if (empty($template_values[31])) {
		# code...
	} else {

		if ($template_values[31] == 'http://') {

		} else {

			$pdf_content .= '
															<div>
																<div><strong>Url fuente de la alerta:</strong>  '.$template_values[31].'</div>
	                                          </div>
	                                          <p></p>
														';

		}

	}

	if (empty($template_values[2])) {
		# code...
	} else {

		$pdf_content .= '

															<div>
																<div><strong>No. Identificación interno: </strong>  '.$template_values[2].'</div>
	                                          </div>
	                                          <p></p>
											    ';
	}

	if (isset($template_values[32])) {

		if (empty($template_values[18])) {
			# code...
			$pdf_content .= '

													<hr>

													<br>

													';
		} else {

			$pdf_content .= '

													<hr>
													<br>
													<br>
													';

		}
		# code...

	} else {

		$pdf_content .= '

														<hr>

													<br>
													 <div style="width:100%;padding-bottom:20px;"></div>

														';

	}

	// imagen
	$pdf_content .= '

																	<center>
																	<div class="alerta_img" style="list-style-type: none;margin: 0;padding: 0;width: 100%;font-family: dejavu sans,Lucida Grande,Tahoma,Arial,Verdana,sans-serif;font-size:9.2pt;position:relative;">'.$imagen_alerta.'
																	</div>
																	</center>
																	<p></p>
																	<p><strong>Descripción del caso</strong></p>
													';

	if ($link_archivos == "&nbsp;") {
		# code...
	} else {
		# code...

		$pdf_content .= '

																<p>En el siguiente enlace podrá revisar el detalle de los productos:
																</p>


																<p align="Justify">'.$link_archivos.'</p>

																				';

	}

	# DM-PF1-Descripción Problema

	if (empty($template_values[34])) {
		# code...
	} else {
		# code...

		$pdf_content .= '


																<p align="Justify">'.$template_values[34].'</p>

																				';

	}

	#DM-PF2-Descripción Problema

	if (empty($template_values[35])) {
		# code...
	} else {
		# code...

		$pdf_content .= '


																<p align="Justify">'.$template_values[35].'</p>

																				';

	}

	if (empty($template_values[36])) {
		# code...
	} else {
		# code...

		$pdf_content .= '


																<p align="Justify">'.$template_values[36].'</p>

																				';

	}
	if (empty($template_values[37])) {
		# code...
	} else {
		# code...

		$pdf_content .= '


																<p align="Justify">'.$template_values[37].'</p>

																				';

	}

	if (empty($template_values[38])) {
		# code...
	} else {
		# code...

		$pdf_content .= '


																<p align="Justify">'.$template_values[38].'</p>

																				';

	}
	if (empty($template_values[39])) {
		# code...
	} else {
		# code...

		$pdf_content .= '


																<p align="Justify">'.$template_values[39].'</p>

																				';

	}
	if (empty($template_values[40])) {
		# code...
	} else {
		# code...

		$pdf_content .= '


																<p align="Justify">'.$template_values[40].'</p>

																				';

	}
	if (empty($template_values[41])) {
		# code...
	} else {
		# code...

		$pdf_content .= '


																<p align="Justify">'.$template_values[41].'</p>

																				';

	}

	if (empty($template_values[42])) {
		# code...
	} else {
		# code...

		$pdf_content .= '


																		<p align="Justify">'.$template_values[42].'</p>

																						';

	}
	if (empty($template_values[43])) {
		# code...
	} else {
		# code...

		$pdf_content .= '


																		<p align="Justify">'.$template_values[43].'</p>

																						';

	}

	if (empty($template_values[44])) {
		# code...
	} else {
		# code...

		$pdf_content .= '


																		<p align="Justify">'.$template_values[44].'</p>

																						';

	}

	if (empty($template_values[45])) {
		# code...
	} else {
		# code...

		$pdf_content .= '


																		<p align="Justify">'.$template_values[45].'</p>

																						';

	}
	if (empty($template_values[46])) {
		# code...
	} else {
		# code...

		$pdf_content .= '


																		<p align="Justify">'.$template_values[46].'</p>

																						';

	}

	if (empty($template_values[47])) {
		# code...
	} else {
		# code...

		$pdf_content .= '


																		<p align="Justify">'.$template_values[47].'</p>

																						';

	}
	if (empty($template_values[48])) {
		# code...
	} else {
		# code...

		$pdf_content .= '


																		<p align="Justify">'.$template_values[48].'</p>

																						';

	}
	if (empty($template_values[49])) {
		# code...
	} else {
		# code...

		$pdf_content .= '


																		<p align="Justify">'.$template_values[49].'</p>

																						';

	}
	if (empty($template_values[50])) {
		# code...
	} else {
		# code...

		$pdf_content .= '


																		<p align="Justify">'.$template_values[50].'</p>

																						';

	}
	if (empty($template_values[51])) {
		# code...
	} else {
		# code...

		$pdf_content .= '


																		<p align="Justify">'.$template_values[51].'</p>

																						';

	}
	if (empty($template_values[52])) {
		# code...
	} else {
		# code...

		$pdf_content .= '


																		<p align="Justify">'.$template_values[52].'</p>

																						';

	}
	if (empty($template_values[53])) {
		# code...
	} else {
		# code...

		$pdf_content .= '


																		<p align="Justify">'.$template_values[53].'</p>

																						';

	}

	if (empty($template_values[54])) {
		# code...
	} else {
		# code...

		$pdf_content .= '


																		<p align="Justify">'.$template_values[54].'</p>

																						';

	}

	if (empty($template_values[55])) {
		# code...
	} else {
		# code...

		//li 1 gris
		$pdf_content .= '

																	<p><strong>Indicaciones y uso establecido</strong></p>
																	<p>
																	<div align="Justify" style="width: 100%;padding:5px 0px">
																	'.$template_values[55].'
																	</div>

											';
	}

	/********************  DM-PF1  ********************/

	if (empty($template_values[56])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																		<p><strong>Medidas para la comunidad en general</strong></p>
																		<p>
																		<div align="Justify" style="width: 100%;padding:5px 0px">
																		'.$template_values[56].'
																		</div>
												';

	}

	if (empty($template_values[57])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																		<p><strong>Medidas para secretarías de salud departamentales, distritales y municipales</strong></p>
																		<p>
																		<div align="Justify" style="width: 100%;padding:5px 0px">
																		'.$template_values[57].'
																		</div>
												';

	}

	if (empty($template_values[58])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																		<p><strong>Medidas para Instituciones Prestadoras de Servicio de Salud - IPS y profesionales de la salud</strong></p>
																		<p>
																		<div align="Justify" style="width: 100%;padding:5px 0px">
																		'.$template_values[58].'
																		</div>
												';

	}

	if (empty($template_values[59])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																			<p><strong>A los establecimientos titulares, distribuidores y comercializadores</strong></p>
																			<p>
																			<div align="Justify" style="width: 100%;padding:5px 0px">
																			'.$template_values[59].'
																			</div>
													';

	}

	if (empty($template_values[60])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																			<p><strong>Medidas para la Red Nacional de Farmacovigilancia</strong></p>
																			<p>
																			<div align="Justify" style="width: 100%;padding:5px 0px">
																			'.$template_values[60].'
																			</div>
													';

	}

	/********************  // DM-PF1   ********************/

	/********************  DM-PF2  ********************/

	if (empty($template_values[61])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																	<p><strong>Medidas para la comunidad en general</strong></p>
																	<p>
																	<div align="Justify" style="width: 100%;padding:5px 0px">
																	'.$template_values[61].'
																	</div>
											';

	}

	if (empty($template_values[62])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																	<p><strong>Medidas para secretarías de salud departamentales, distritales y municipales </strong></p>
																	<p>
																	<div align="Justify" style="width: 100%;padding:5px 0px">
																	'.$template_values[62].'
																	</div>
											';

	}

	if (empty($template_values[63])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																	<p><strong>A los establecimientos titulares, distribuidores y comercializadores</strong></p>
																	<p>
																	<div align="Justify" style="width: 100%;padding:5px 0px">
																	'.$template_values[63].'
																	</div>
											';

	}

	if (empty($template_values[64])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																	<p><strong>Medidas para Instituciones Prestadoras de Servicio de Salud - IPS y profesionales de la salud </strong></p>
																	<p>
																	<div align="Justify" style="width: 100%;padding:5px 0px">
																	'.$template_values[64].'
																	</div>
											';

	}

	if (empty($template_values[65])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																	<p><strong>Medidas para la Red Nacional de Farmacovigilancia</strong></p>
																	<p>
																	<div align="Justify" style="width: 100%;padding:5px 0px">
																	'.$template_values[65].'
																	</div>
											';

	}

	/********************  // DM-PF2    ********************/

	/********************  DM-PF3  ********************/

	if (empty($template_values[66])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																		<p><strong>Medidas para la comunidad en general</strong></p>
																		<p>
																		<div align="Justify" style="width: 100%;padding:5px 0px">
																		'.$template_values[66].'
																		</div>
												';

	}

	if (empty($template_values[67])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																		<p><strong>Medidas para secretarías de salud departamentales, distritales y municipales </strong></p>
																		<p>
																		<div align="Justify" style="width: 100%;padding:5px 0px">
																		'.$template_values[67].'
																		</div>
												';

	}

	if (empty($template_values[68])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																	<p><strong>Medidas para Instituciones Prestadoras de Servicio de Salud - IPS y profesionales de la salud </strong></p>
																	<p>
																	<div align="Justify" style="width: 100%;padding:5px 0px">
																	'.$template_values[68].'
																	</div>
											';

	}

	if (empty($template_values[69])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																	<p><strong>A los establecimientos titulares, distribuidores y comercializadores</strong></p>
																	<p>
																	<div align="Justify" style="width: 100%;padding:5px 0px">
																	'.$template_values[69].'
																	</div>
											';

	}

	if (empty($template_values[70])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																	<p><strong>Medidas para la Red Nacional de Farmacovigilancia</strong></p>
																	<p>
																	<div align="Justify" style="width: 100%;padding:5px 0px">
																	'.$template_values[70].'
																	</div>
											';

	}

	/********************  // DM-PF3    ********************/

	/********************  DM-PA1  ********************/

	if (empty($template_values[71])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																		<p><strong>Medidas para la comunidad en general</strong></p>
																		<p>
																		<div align="Justify" style="width: 100%;padding:5px 0px">
																		'.$template_values[71].'
																		</div>
												';

	}

	if (empty($template_values[72])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																		<p><strong>Medidas para secretarías de salud departamentales, distritales y municipales</strong></p>
																		<p>
																		<div align="Justify" style="width: 100%;padding:5px 0px">
																		'.$template_values[72].'
																		</div>
												';

	}

	if (empty($template_values[73])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																		<p><strong>Medidas para Instituciones Prestadoras de Servicio de Salud - IPS y profesionales de la salud </strong></p>
																		<p>
																		<div align="Justify" style="width: 100%;padding:5px 0px">
																		'.$template_values[73].'
																		</div>
												';

	}

	if (empty($template_values[74])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																			<p><strong>A los establecimientos titulares, distribuidores y comercializadores</strong></p>
																			<p>
																			<div align="Justify" style="width: 100%;padding:5px 0px">
																			'.$template_values[74].'
																			</div>
													';

	}

	/********************  // DM-PA1    ********************/

	/********************  DM-PA2  ********************/

	if (empty($template_values[75])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																		<p><strong>Medidas para la comunidad en general</strong></p>
																		<p>
																		<div align="Justify" style="width: 100%;padding:5px 0px">
																		'.$template_values[75].'
																		</div>
												';

	}

	if (empty($template_values[76])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																		<p><strong>Medidas para secretarías de salud departamentales, distritales y municipales </strong></p>
																		<p>
																		<div align="Justify" style="width: 100%;padding:5px 0px">
																		'.$template_values[76].'
																		</div>
												';

	}

	if (empty($template_values[77])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																		<p><strong>Medidas para Instituciones Prestadoras de Servicio de Salud - IPS y profesionales de la salud </strong></p>
																		<p>
																		<div align="Justify" style="width: 100%;padding:5px 0px">
																		'.$template_values[77].'
																		</div>
												';

	}

	if (empty($template_values[78])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																			<p><strong>A los establecimientos titulares, distribuidores y comercializadores</strong></p>
																			<p>
																			<div align="Justify" style="width: 100%;padding:5px 0px">
																			'.$template_values[78].'
																			</div>
													';

	}

	if (empty($template_values[79])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																			<p><strong>Medidas para la Red Nacional de Farmacovigilancia</strong></p>
																			<p>
																			<div align="Justify" style="width: 100%;padding:5px 0px">
																			'.$template_values[79].'
																			</div>
													';

	}

	/********************  // DM-PA2    ********************/

	/********************  DM-PA3  ********************/

	if (empty($template_values[80])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																	<p><strong>Medidas para la comunidad en general</strong></p>
																	<p>
																	<div align="Justify" style="width: 100%;padding:5px 0px">
																	'.$template_values[80].'
																	</div>
											';

	}

	/********************  // DM-PA3    ********************/

	/********************  DD-PF1 ********************/

	if (empty($template_values[81])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																	<p><strong>Medidas para la comunidad en general</strong></p>
																	<p>
																	<div align="Justify" style="width: 100%;padding:5px 0px">
																	'.$template_values[81].'
																	</div>
											';

	}

	if (empty($template_values[82])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																	<p><strong>Medidas para secretarías de salud departamentales, distritales y municipales </strong></p>
																	<p>
																	<div align="Justify" style="width: 100%;padding:5px 0px">
																	'.$template_values[82].'
																	</div>
											';

	}

	if (empty($template_values[83])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																	<p><strong>Medidas para Instituciones Prestadoras de Servicio de Salud - IPS y profesionales de la salud </strong></p>
																	<p>
																	<div align="Justify" style="width: 100%;padding:5px 0px">
																	'.$template_values[83].'
																	</div>
											';

	}

	if (empty($template_values[84])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																		<p><strong>A los establecimientos titulares, distribuidores y comercializadores</strong></p>
																		<p>
																		<div align="Justify" style="width: 100%;padding:5px 0px">
																		'.$template_values[84].'
																		</div>
												';

	}

	if (empty($template_values[85])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																		<p><strong>Medidas para la Red Nacional de '.$template_values[136].'</strong></p>
																		<p>
																		<div align="Justify" style="width: 100%;padding:5px 0px">
																		'.$template_values[85].'
																		</div>
												';

	}

	/********************  // DD-PF1    ********************/

	/********************  DD-PF2 ********************/

	if (empty($template_values[86])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																	<p><strong>Medidas para la comunidad en general</strong></p>
																	<p>
																	<div align="Justify" style="width: 100%;padding:5px 0px">
																	'.$template_values[86].'
																	</div>
											';

	}

	if (empty($template_values[87])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																	<p><strong>Medidas para Instituciones Prestadoras de Servicio de Salud - IPS y profesionales de la salud </strong></p>
																	<p>
																	<div align="Justify" style="width: 100%;padding:5px 0px">
																	'.$template_values[87].'
																	</div>
											';

	}

	if (empty($template_values[88])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																		<p><strong>A los establecimientos titulares, distribuidores y comercializadores</strong></p>
																		<p>
																		<div align="Justify" style="width: 100%;padding:5px 0px">
																		'.$template_values[88].'
																		</div>
												';

	}

	if (empty($template_values[89])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																		<p><strong>Medidas para la Red Nacional de '.$template_values[136].'</strong></p>
																		<p>
																		<div align="Justify" style="width: 100%;padding:5px 0px">
																		'.$template_values[89].'
																		</div>
												';

	}

	/********************  // DD-PF2    ********************/

	/********************  DD-PA1 ********************/

	if (empty($template_values[90])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																	<p><strong>Medidas para la comunidad en general</strong></p>
																	<p>
																	<div align="Justify" style="width: 100%;padding:5px 0px">
																	'.$template_values[90].'
																	</div>
											';

	}

	if (empty($template_values[91])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																	<p><strong>Medidas para secretarías de salud departamentales, distritales y municipales </strong></p>
																	<p>
																	<div align="Justify" style="width: 100%;padding:5px 0px">
																	'.$template_values[91].'
																	</div>
											';

	}

	if (empty($template_values[92])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																	<p><strong>Medidas para Instituciones Prestadoras de Servicio de Salud - IPS y profesionales de la salud </strong></p>
																	<p>
																	<div align="Justify" style="width: 100%;padding:5px 0px">
																	'.$template_values[92].'
																	</div>
											';

	}

	if (empty($template_values[93])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																	<p><strong>A los establecimientos titulares, distribuidores y comercializadores</strong></p>
																	<p>
																	<div align="Justify" style="width: 100%;padding:5px 0px">
																	'.$template_values[93].'
																	</div>
											';

	}

	if (empty($template_values[94])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																	<p><strong>Medidas para la Red Nacional de '.$template_values[136].'</strong></p>
																	<p>
																	<div align="Justify" style="width: 100%;padding:5px 0px">
																	'.$template_values[94].'
																	</div>
											';

	}

	/********************  // DD-PA1    ********************/

	/********************  DD-PA2 ********************/

	if (empty($template_values[95])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																	<p><strong>Medidas para la comunidad en general</strong></p>
																	<p>
																	<div align="Justify" style="width: 100%;padding:5px 0px">
																	'.$template_values[95].'
																	</div>
											';

	}

	if (empty($template_values[96])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																	<p><strong>Medidas para secretarías de salud departamentales, distritales y municipales</strong></p>
																	<p>
																	<div align="Justify" style="width: 100%;padding:5px 0px">
																	'.$template_values[96].'
																	</div>
											';

	}

	if (empty($template_values[97])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																	<p><strong>Medidas para Instituciones Prestadoras de Servicio de Salud - IPS y profesionales de la salud </strong></p>
																	<p>
																	<div align="Justify" style="width: 100%;padding:5px 0px">
																	'.$template_values[97].'
																	</div>
											';

	}

	if (empty($template_values[98])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																	<p><strong>A los establecimientos titulares, distribuidores y comercializadores</strong></p>
																	<p>
																	<div align="Justify" style="width: 100%;padding:5px 0px">
																	'.$template_values[98].'
																	</div>
											';

	}

	if (empty($template_values[99])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																	<p><strong>Medidas para la Red Nacional de '.$template_values[136].'</strong></p>
																	<p>
																	<div align="Justify" style="width: 100%;padding:5px 0px">
																	'.$template_values[99].'
																	</div>
											';

	}

	/********************  // DD-PA2    ********************/

	/********************  DD-DC ********************/

	if (empty($template_values[100])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																	<p><strong>Medidas para la comunidad en general</strong></p>
																	<p>
																	<div align="Justify" style="width: 100%;padding:5px 0px">
																	'.$template_values[100].'
																	</div>
											';

	}

	if (empty($template_values[101])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																	<p><strong>Medidas para Instituciones Prestadoras de Servicio de Salud - IPS y profesionales de la salud </strong></p>
																	<p>
																	<div align="Justify" style="width: 100%;padding:5px 0px">
																	'.$template_values[101].'
																	</div>
											';

	}

	if (empty($template_values[102])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																		<p><strong>A los establecimientos titulares, distribuidores y comercializadores</strong></p>
																		<p>
																		<div align="Justify" style="width: 100%;padding:5px 0px">
																		'.$template_values[102].'
																		</div>
												';

	}

	if (empty($template_values[103])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																		<p><strong>Medidas para la Red Nacional de '.$template_values[136].'</strong></p>
																		<p>
																		<div align="Justify" style="width: 100%;padding:5px 0px">
																		'.$template_values[103].'
																		</div>
												';

	}

	/********************  // DD-DC    ********************/

	/********************  DC-PF1 ********************/

	if (empty($template_values[104])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																	<p><strong>Medidas para la comunidad en general</strong></p>
																	<p>
																	<div align="Justify" style="width: 100%;padding:5px 0px">
																	'.$template_values[104].'
																	</div>
											';

	}

	if (empty($template_values[105])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																	<p><strong>Medidas para secretarías de salud departamentales, distritales y municipales </strong></p>
																	<p>
																	<div align="Justify" style="width: 100%;padding:5px 0px">
																	'.$template_values[105].'
																	</div>
											';

	}

	if (empty($template_values[106])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																		<p><strong>A los establecimientos titulares, distribuidores y comercializadores</strong></p>
																		<p>
																		<div align="Justify" style="width: 100%;padding:5px 0px">
																		'.$template_values[106].'
																		</div>
												';

	}

	/********************  // DD-PF1    ********************/

	/********************  DC-PF2 ********************/

	if (empty($template_values[107])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																	<p><strong>Medidas para la comunidad en general</strong></p>
																	<p>
																	<div align="Justify" style="width: 100%;padding:5px 0px">
																	'.$template_values[107].'
																	</div>
											';

	}

	if (empty($template_values[108])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																	<p><strong>Medidas para secretarías de salud departamentales, distritales y municipales</strong></p>
																	<p>
																	<div align="Justify" style="width: 100%;padding:5px 0px">
																	'.$template_values[108].'
																	</div>
											';

	}

	/////// proceso de arreglo

	if (empty($template_values[110])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																		<p><strong>A los establecimientos titulares, distribuidores y comercializadores</strong></p>
																		<p>
																		<div align="Justify" style="width: 100%;padding:5px 0px">
																		'.$template_values[110].'
																		</div>
												';

	}

	/********************  // DD-PF2    ********************/

	/********************  DC-PA1 ********************/

	if (empty($template_values[111])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																	<p><strong>Medidas para la comunidad en general</strong></p>
																	<p>
																	<div align="Justify" style="width: 100%;padding:5px 0px">
																	'.$template_values[111].'
																	</div>
											';

	}

	if (empty($template_values[112])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																	<p><strong>Medidas para secretarías de salud departamentales, distritales y municipales </strong></p>
																	<p>
																	<div align="Justify" style="width: 100%;padding:5px 0px">
																	'.$template_values[112].'
																	</div>
											';

	}

	if (empty($template_values[112])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																		<p><strong>A los establecimientos titulares, distribuidores y comercializadores</strong></p>
																		<p>
																		<div align="Justify" style="width: 100%;padding:5px 0px">
																		'.$template_values[112].'
																		</div>
												';

	}

	/********************  // DD-PA1    ********************/

	/********************  DC-PA2 ********************/

	if (empty($template_values[113])){
		# code...
	} else {
		# code...

		//li 2
		$pdf_content .= '

																	<p><strong>Medidas para la comunidad en general</strong></p>
																	<p>
																	<div align="Justify" style="width: 100%;padding:5px 0px">
																	'.$template_values[113].'
																	</div>
											';

	}

	if (empty($template_values[114])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																	<p><strong>Medidas para secretarías de salud departamentales, distritales y municipales </strong></p>
																	<p>
																	<div align="Justify" style="width: 100%;padding:5px 0px">
																	'.$template_values[114].'
																	</div>
											';

	}

	if (empty($template_values[115])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																		<p><strong>A los establecimientos titulares, distribuidores y comercializadores</strong></p>
																		<p>
																		<div align="Justify" style="width: 100%;padding:5px 0px">
																		'.$template_values[115].'
																		</div>
												';

	}

	/********************  // DD-PA2    ********************/

	/********************  DC-PA3 ********************/

	if (empty($template_values[116])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																	<p><strong>Medidas para la comunidad en general</strong></p>
																	<p>
																	<div align="Justify" style="width: 100%;padding:5px 0px">
																	'.$template_values[116].'
																	</div>
											';

	}

	/********************  DC-PA3 ********************/

	/********************  DA-PF1 ********************/

	if (empty($template_values[117])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																	<p><strong>Medidas para la comunidad en general</strong></p>
																	<p>
																	<div align="Justify" style="width: 100%;padding:5px 0px">
																	'.$template_values[117].'
																	</div>
											';

	}

	if (empty($template_values[118])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																		<p><strong>Medidas para secretarías de salud departamentales, distritales y municipales </strong></p>
																		<p>
																		<div align="Justify" style="width: 100%;padding:5px 0px">
																		'.$template_values[118].'
																		</div>
												';

	}

	/********************  // DA-PF1    ********************/

	/********************  DA-PF2 ********************/

	if (empty($template_values[119])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																		<p><strong>Medidas para la comunidad en general</strong></p>
																		<p>
																		<div align="Justify" style="width: 100%;padding:5px 0px">
																		'.$template_values[119].'
																		</div>
												';

	}

	if (empty($template_values[120])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																		<p><strong>Medidas para secretarías de salud departamentales, distritales y municipales </strong></p>
																		<p>
																		<div align="Justify" style="width: 100%;padding:5px 0px">
																		'.$template_values[120].'
																		</div>
												';

	}

	/********************  // DA-PF2    ********************/

	//********************  DA-PF3 ********************/

	if (empty($template_values[121])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																		<p><strong>Medidas para la comunidad en general</strong></p>
																		<p>
																		<div align="Justify" style="width: 100%;padding:5px 0px">
																		'.$template_values[121].'
																		</div>
												';

	}

	if (empty($template_values[122])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																		<p><strong>Medidas para secretarías de salud departamentales, distritales y municipales</strong></p>
																		<p>
																		<div align="Justify" style="width: 100%;padding:5px 0px">
																		'.$template_values[122].'
																		</div>
												';

	}

	/********************  // DA-PF3    ********************/

	/********************  DA-PCAAF ********************/

	if (empty($template_values[123])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																		<p><strong>Medidas para la comunidad en general</strong></p>
																		<p>
																		<div align="Justify" style="width: 100%;padding:5px 0px">
																		'.$template_values[123].'
																		</div>
												';

	}

	if (empty($template_values[124])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																		<p><strong>Medidas para secretarías de salud departamentales, distritales y municipales </strong></p>
																		<p>
																		<div align="Justify" style="width: 100%;padding:5px 0px">
																		'.$template_values[124].'
																		</div>
												';

	}

	/********************  // DA-PCAAF    ********************/

	/********************  DA-AC ********************/

	if (empty($template_values[125])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																		<p><strong>Medidas para la comunidad en general</strong></p>
																		<p>
																		<div align="Justify" style="width: 100%;padding:5px 0px">
																		'.$template_values[125].'
																		</div>
												';

	}

	if (empty($template_values[126])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																	<br>
																	<center>
																	<div class="alerta_img_cg">'.$template_values[126].'
																	</div>
																	</center>
																	<p>
												';

	}

	if (empty($template_values[127])) {
		# code...
	} else {
		# code...

		//li 2 blanco
		$pdf_content .= '

																		<p><strong>Medidas para secretarías de salud departamentales, distritales y municipales </strong></p>
																		<p>
																		<div align="Justify" style="width: 100%;padding:5px 0px">
																		'.$template_values[127].'
																		</div>
												';

	}

	/********************  // DA-AC    ********************/

	if (empty($template_values[128])) {
		# code...
	} else {
		# code..
		$pdf_content .= '
															<div style="padding:5px 0px;"><strong>Si desea obtener mayor información comuníquese con el Invima a: </strong></div>
															<p>
									';

		$pdf_content .= '
															<div style="padding:5px 0px;color:#11576C;">'.$template_values[128].'</div>
															<p>
									';
	}

	$pdf_content .= '<div style="padding:5px 0px;"><strong>'.$template_values[129].'</strong></div>';

	$pdf_content .= '	<ul>
														<li>
															<div style="wpadding:5px 0px;"><a style="color:#11576C" href="https://www.invima.gov.co/peticiones-quejas-reclamos-y-sugerencias" target="_blank" ><strong>Realizar peticiones, quejas, reclamos, denuncias o sugerencias </strong></a> </div>
														</li>
									';

	$pdf_content .= '
														<li>
															<div style="padding:5px 0px;"><strong><a style="color:#11576C" href="http://consultaregistro.invima.gov.co:8082/Consultas/consultas/consreg_encabcum.jsp" target="_blank" >Consultar registros sanitarios</a></strong></div>
														</li>
													</ul>
									';

	if (empty($template_values[128])) {
		# code...
	} else {
		# code..
		$pdf_content .= '
														<ul>
															<div style="padding:5px 0px;"><strong>Realizar reportes en línea de eventos adversos</strong></div>
															<p>
									';

		$pdf_content .= '	<ul>
														<li>
															<div style="wpadding:5px 0px;"><a style="color:#11576C" href="https://farmacoweb.invima.gov.co/reportesfv/login/loginUsuario.jsp" target="_blank" ><strong>Farmacovigilancia </strong></a> </div>
														</li>
									';

		$pdf_content .= '
														<li>
															<div style="padding:5px 0px;"><strong><a style="color:#11576C" href="https://reactivoenlinea.invima.gov.co/ReactivoVigilanciaWeb/" target="_blank" >Reactivovigilancia</a></strong></div>
														</li>
									';

		$pdf_content .= '
														<li>
															<div style="padding:5px 0px;"><strong><a style="color:#11576C" href="https://farmacoweb.invima.gov.co/TecnoVigilancia/" target="_blank" >Tecnovigilancia</a></strong></div>
														</li>
													</ul>
													</ul>
									';
	}

	$pdf_content .= '
									</body>
								</html>
										';
	// Variable para imprimir {entry_data}

	$pdf_content_footer = '

												<img style="position: absolute; bottom:50; width:99%;" align="center" width="100%"  src="./images/alertas_img/img_pie.png">

									';

	/* fin de variable de alerta sanitaria */

	//parse pdf template
	$pdf_content = str_replace($template_variables, $template_values, $pdf_content);

	//print_r($pdf_content);

	//exit();
	//generate PDF file
	$dompdf = new DOMPDF();

	//paper size: letter, legal, ledger, tabloid, executive, folio, a0, a1, a2, a3, a4,a5, a6, etc
	//orientation: portrait, landscape
	$dompdf->set_paper('A4', 'portrait');

	$html_pdf = $pdf_content;

	//print_r($pdf_content);
	//exit();

	$dompdf->load_html($html_pdf);

	$dompdf->render();

	$canvas = $dompdf->get_canvas();

	//For header
	$header = $canvas->open_object();
	$font   = Font_Metrics::get_font("Arial", "bold");
	$date   = date("Y-m-d H:i:s");
	//$canvas->page_text(35, 25, "Invima", $font, 8, array(0, 0, 0));
	//$canvas->page_text(470, 25, "Alerta Invima No. $template_values[1]", $font, 8, array(0, 0, 0));
	$canvas->close_object();
	$canvas->add_object($header, "all");

	//For Footer
	$footer = $canvas->open_object();
	$font   = Font_Metrics::get_font("Arial", "bold");
	$date   = date("Y-m-d H:i:s");
	$image  = "./images/alertas_img/img_pie.png";
	$canvas->image($image, 0, 720, 600, 80);
	$canvas->page_text(250, 800, "Página: {PAGE_NUM} de {PAGE_COUNT}", $font, 8, array(0, 0, 0));
	$canvas->page_text(240, 820, "Alerta Invima No. $template_values[1]", $font, 8, array(0, 0, 0));
	//$canvas->page_text(500, 800, $date, $font, 8, array(0, 0, 0));
	$canvas->close_object();
	$canvas->add_object($footer, "all");

	//$dompdf->stream("Alerta Sanitaria Numero #{$entry_id} - Form #{$form_id}.pdf");
	$dompdf->stream("Alerta No. #$template_values[1] - $template_values[17] .pdf");

}

?>
