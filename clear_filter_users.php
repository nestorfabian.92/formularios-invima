<?php
/********************************************************************************
 AppForm Invima
  
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
 
 ********************************************************************************/
 
	require('includes/init.php');

	require('config.php');
	require('includes/db-core.php');
	require('includes/helper-functions.php');
	require('includes/check-session.php');

	//check user privileges, is this user has privilege to administer appForm?
	if(empty($_SESSION['mf_user_privileges']['priv_administer'])){
		die("You don't have permission to administer AppForm.");
	}

	$_SESSION['filter_users'] = array();
	unset($_SESSION['filter_users']);
	
	
	$response_data = new stdClass();
	$response_data->status    	= "ok";
	
	
	$response_json = json_encode($response_data);
	
	echo $response_json;
?>