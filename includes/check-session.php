<?php
/********************************************************************************
 AppForm invima

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

 ********************************************************************************/

	//check if user logged in or not
	//if not redirect them into login page

	//first we need to check if the user has "remember me" cookie or not
	if(!empty($_COOKIE['mf_remember']) && empty($_SESSION['mf_logged_in'])){
		$dbh	= mf_connect_db();
		$query  = "SELECT 
						`user_id`,
						`priv_administer`,
						`priv_new_forms`,
						`priv_new_themes` 
					FROM 
						`".MF_TABLE_PREFIX."users` 
					WHERE 
						`cookie_hash`=? and `status`=1";
		$params = array($_COOKIE['mf_remember']);
		
		$sth = mf_do_query($query,$params,$dbh);
		$row = mf_do_fetch_result($sth);

		$mf_user_id 		  = $row['user_id'];
		$mf_priv_administer	  = (int) $row['priv_administer'];
		$mf_priv_new_forms	  = (int) $row['priv_new_forms'];
		$mf_priv_new_themes	  = (int) $row['priv_new_themes'];

		if(!empty($mf_user_id)){
			$_SESSION['mf_logged_in'] = true;
			$_SESSION['mf_user_id']	  = $mf_user_id;
			$_SESSION['mf_user_privileges']['priv_administer'] = $mf_priv_administer;
			$_SESSION['mf_user_privileges']['priv_new_forms']  = $mf_priv_new_forms;
			$_SESSION['mf_user_privileges']['priv_new_themes'] = $mf_priv_new_themes;
		}

	}

	if(empty($_SESSION['mf_logged_in'])){
		$ssl_suffix  = mf_get_ssl_suffix();
		
		$current_dir = dirname($_SERVER['PHP_SELF']);
      	if($current_dir == "/" || $current_dir == "\\"){
			$current_dir = '';
		}
		
		$_SESSION['MF_LOGIN_ERROR'] = 'Your session has expired. Please login.';
		header("Location: http{$ssl_suffix}://".$_SERVER['HTTP_HOST'].$current_dir.'/index.php?from='.base64_encode($_SERVER['REQUEST_URI']));
		exit;
	}
	
?>