<?php
/********************************************************************************
 AppForm invima

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

 ********************************************************************************/

	session_start();

	date_default_timezone_set(@date_default_timezone_get());	
	error_reporting(E_ALL & ~E_STRICT & ~E_NOTICE);
	
	@header("Content-Type: text/html; charset=UTF-8");
?>