<!--

/********************************************************************************
 AppForm invima

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

 ********************************************************************************/


-->

<div class="clear"></div>
	
	</div><!-- /#main -->
	<img src="images/bottom.png" id="bottom_shadow">
	<div id="footer">
		
		<p class="copyright"><a href="https://sourceforge.net/p/appform/wiki/">AppFom GNU/GPL</a></p>
		<div class="clear"></div>
		
	</div><!-- /#footer -->
	

</div><!-- /#container -->

</div><!-- /#bg -->

<?php
	if($disable_jquery_loading !== true){
		echo '<script type="text/javascript" src="js/jquery.min.js"></script>';
	}
?>

<?php if(!empty($footer_data)){ echo $footer_data; } ?>
</body>
</html>