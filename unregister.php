<?php
/********************************************************************************
 AppForm invima

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

 ********************************************************************************/
 
	require('includes/init.php');
	
	require('config.php');
	require('includes/db-core.php');
	require('includes/helper-functions.php');
	require('includes/check-session.php');
	
	if(empty($_POST['unregister'])){
		die("Invalid parameters.");
	}

	//check user privileges, is this user has privilege to administer AppForm
	if(empty($_SESSION['mf_user_privileges']['priv_administer'])){
		die("Access Denied. You don't have permission to administer AppForm.");
	}
	
	$dbh = mf_connect_db();
	$data['customer_name'] = 'unregistered';
	$data['customer_id']   = '';
	$data['license_key']   = '';

   	mf_ap_settings_update($data,$dbh);

   	echo '{"status" : "ok"}';
?>