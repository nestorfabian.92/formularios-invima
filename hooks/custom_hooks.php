<?php
/********************************************************************************
 AppForm invima

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

 ********************************************************************************/


global $mf_hook_emails;

/*** START SAMPLE - Sample email hooks

This hooks variable can be used to dynamically set the destination email address of a form
based on dropdown selection

For example, you have a form with id number = 3 and dropdown with element_id = 5 and have three options:
- First option
- Second option
- Third option
your hook variable should be the following:

START EMAIL HOOK ---------------------

$mf_hook_emails[3]['element_id'] = 5;

$mf_hook_emails[3]['First option']   = 'email_1@example.com';
$mf_hook_emails[3]['Second option']	 = 'email_2@example.com';
$mf_hook_emails[3]['Third option'] 	 = 'email_3@example.com,email_4@example.com';	

END EMAIL HOOK ---------------------

END SAMPLE ****/


?>